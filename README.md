# Backtest Utilities Library - Python

Python 3.7.x+ Backtest Utilities Library.

## Getting Started

### Installation
#### Ubuntu 18.04 (Debian-based Linux)
```shell script
python3.7 -m pip install git+ssh://git@gitlab.com/<path>

python3.7 -m pip install git+ssh://git@gitlab.com/dodo-trader/trader-utilities/backtest-utilities-library-python
```
```shell script
python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>

python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/dodo-trader/trader-utilities/backtest-utilities-library-python
```
#### Windows 10
```shell script
pip install git+ssh://git@gitlab.com/<path>

pip install git+ssh://git@gitlab.com/dodo-trader/trader-utilities/backtest-utilities-library-python
```
```shell script
pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>

pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/dodo-trader/trader-utilities/backtest-utilities-library-python
```
#### Requirements.txt
For development of other projects
```
backtestutilitiespy@ git+ssh://git@gitlab.com/dodo-trader/trader-utilities/backtest-utilities-library-python
```
