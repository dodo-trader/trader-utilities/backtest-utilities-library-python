from setuptools import setup, find_packages

version = {}
with open("backtestutilitiespy/version.py") as file:
    exec(file.read(), version)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="backtestutilitiespy",
    version=version['__version__'],
    author="Ernest Yuen",
    author_email="ernestyuen08@gmail.com",
    description="Backtest Utilities Library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/dodo-trader/trader-utilities/backtest-utilities-library-python",
    packages=find_packages(),
    install_requires=[
        "networkandutilitiespy@ git+ssh://git@gitlab.com/ernest-libraries/network-and-utilities-library-python"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
