import logging

from networkandutilitiespy.utils import custom_logger

from .util import *


log = custom_logger(__name__, filename='./interday_trading_candlestick_template_util.log',
                    mode='w+', log_level=logging.INFO)

RESOLUTION_DICT = {
    'daily': 'D',
    'weekly': 'W',
    'monthly': 'M',
    'quarterly': 'Q',
    'yearly': 'Y'
}


def generate_backtest_code(resolution: str, no_of_eng_char: int, no_of_digits: int,
                           instrument_code: str, direction: str, timeframe: str):
    if direction.upper() == 'LONG':
        direction = 'L'
    elif direction.upper() == 'SHORT':
        direction = 'S'
    else:
        log.error(f'The direction is invalid.')

    if RESOLUTION_DICT[resolution] is not None:
        return f'{RESOLUTION_DICT[resolution]}{random_characters(no_of_eng_char=no_of_eng_char)}' \
               f'{random_digits(no_of_digits=no_of_digits)}@{instrument_code}*{direction}{timeframe}'
    else:
        return f'{resolution}{random_characters(no_of_eng_char=no_of_eng_char)}' \
               f'{random_digits(no_of_digits=no_of_digits)}@{instrument_code}*{direction}{timeframe}'


def strategy_code_decomposition(strategy_code):

    def fetch_front(code):
        for i in range(len(code)):
            if code[i].isalpha():
                return code[0:i + 1]

    temp = strategy_code.split('*')
    instrument_symbol = temp[0].split('#')[1]
    action = temp[1][0]
    interval = fetch_front(strategy_code)
    no_of_bars = temp[1][1:-1]
    bar_resolution = temp[1][-1]
    random_code = temp[0].split('#')[0][len(interval):]

    return interval, random_code, instrument_symbol, action, no_of_bars, bar_resolution


def strategy_idea_dict(strategy_code: str, description: str, interval: str, stop_loss: str, entry_level: str,
                       profit_level: str, no_of_bars: int, action: str, instrument_symbol: str, bar_resolution: str):
    if action != 'LONG' or action != 'SHORT':
        log.error('The action parameter is invalid.')

    return {
        'strategy_code': strategy_code,
        'description': description,
        'interval': interval,
        'stop_loss': stop_loss,
        'entry_level': entry_level,
        'profit_level': profit_level,
        'no_of_bars': no_of_bars,
        'action': action,
        'instrument_symbol': instrument_symbol,
        'bar_resolution': bar_resolution
    }


def performance_dict(strategy_code: str, ernest_ratio: float, sharpe_ratio: float, annual_return: float,
                     sortino_ratio: float, calmar_ratio: float, maxi_drawdown: float, frequency: int,
                     win_rate: float, time_to_recover: int, description: str, last_backtest_time: int):
    return {
        'strategy_code': strategy_code,
        'ernest_ratio': ernest_ratio,
        'sharpe_ratio': sharpe_ratio,
        'annual_return': annual_return,
        'sortino_ratio': sortino_ratio,
        'calmar_ratio': calmar_ratio,
        'maxi_drawdown': maxi_drawdown,
        'frequency': frequency,
        'win_rate': win_rate,
        'time_to_recover': time_to_recover,
        'description': description,
        'last_backtest_time': last_backtest_time
    }


__all__ = (
    'RESOLUTION_DICT',
    'generate_backtest_code',
    'strategy_code_decomposition',
    'strategy_idea_dict',
    'performance_dict'
)
