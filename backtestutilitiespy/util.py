import random
import string


def random_characters(no_of_eng_char: int):
    return ''.join(random.choice(string.ascii_letters) for letter in range(no_of_eng_char))


def random_digits(no_of_digits: int):
    return random.randrange(start=pow(10, no_of_digits), stop=pow(10, no_of_digits+1))


__all__ = (
    'random_characters',
    'random_digits'
)
